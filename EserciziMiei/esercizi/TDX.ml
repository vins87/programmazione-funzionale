print_string "ciao arianna\n";;
let predsuc n = succ (pred (pred n));;

let rec gcd(m,n) = if m=0 then n
										else gcd(n mod m,m);;

let rec gcd =
	function (0,n) -> n
				 | (m,n) -> gcd(n mod m,m);;


(*somma*)
let somma a b = a+b;;
let s = somma 3 2;;
print_string "\nsomma = ";;
print_int s;;

(*-------------GRUPPO 1--------------*)

(*esercizio 1*)
let pi = 3.14159;;
let area x = pi *. x;;
let pi = 0.0;;
let x = "pippo";;

let a = area 3.0;;
print_char '\n';;
print_float a;;

let a b=3+b;;
a 5;;
a 7;;
let c = a 8;;
print_char '\n';;
print_int c;;

(*esercizio 2*)
let y = 100;;
let x = 5;;
let h x = y+x;;
let y = 0;;

let f = h 7;;
print_char '\n';;
print_int f;;

(*esercizio 3*)
let punct x = x = '.' || x =',' || x = ';';;

(*esercizio 4*)
let pi1 (x,_,_,_) = x;;
let pi2 (_,x,_,_) = x;;
let pi3 (_,_,x,_) = x;;
let pi4 (_,_,_,x) = x;;
let quadrupla = (5,('c',"antonio",(),if 3>4 then 0 else 1),"pippo",true);;
pi2 quadrupla;;
pi3 (pi2 quadrupla);;
pi4 (pi2 quadrupla);;
let quintupla = (3,'a',false,"ciao");;  
pi2 quintupla;; (*non funziona con 5 elementi perch� le funzioni pi1,pi2,pi3,pi4 accettano solo 4 elementi*)

(*esercizio 5*)
(*
E;;
not E;;
E && F;;
(not E) || F;;
E||F;;

*)
(*-------------GRUPPO 2--------------*)

(*----Esercizi 2-----*)
(*esercizio 1*)
let ultime_cifre x =
	let y=abs x in
	(((y/10) mod 10),
	y mod 10);;

ultime_cifre (5);;

(*esercizio 2*)
(* Una cifra � bella se � 0, 3, 7; un numero � bello se la sua ultima cifra � bella
e la penultima (se esiste) non lo �. Quindi in particolare le cifre belle sono
numeri belli. Definire un predicato bello: int -> bool che determini
se un numero � bello. La funzione non deve mai sollevare eccezioni, ma
riportare sempre un bool.*)
let cifra_bella x= x=0 || x=3 || x=7;;
let numero_bello n = 
	let y = n/10 mod 10 in
	(y=0 || (not(cifra_bella ((y))))) && cifra_bella (n mod 10);;

numero_bello 70;;

(*esercizio 3*)
let data (d,m) =
	d>0 &&
	match m with "novembre" | "aprile" | "giugno" | "settembre" -> d<=30
	| "febbraio" -> d<=28
	| "gennaio" | "marzo" | "maggio" | "luglio" | "agosto" | "ottobre" | "dicembre" -> d<=31
	| _-> false;;

data (29,"febbraio");;

(*-------------GRUPPO 3--------------*)
(*esercizio 1*)
(* Rappresentiamo le ore della giornata mediante coppie (h,m): int * int,
dove h � compreso tra 0 e 23, inclusi (le ore) e m � compreso tra 0 e 59,
inclusi (i minuti). Scrivere un programma con una funzione
somma_ore: (int * int) -> (int * int) -> int * int,
che calcoli la somma di due ore cos� rappresentate.
Ad esempio:
somma_ore (3,15) (4,20) = (7,35)
somma_ore (3,45) (4,20) = (8,5)
somma_ore (23,45)(0,20) = (0,5)
Se uno dei due argomenti non � la rappresentazione corretta di un'ora, la
funzione sollever� un'eccezione.
Ricordarsi, anche in questo esercizio, le funzioni predefinite di OCaml per
calcolare il modulo e la divisione intera.
*)
let verifica_ore_min (h,m) =
	h>=0 && h<24 && m>=0 && m<60;;

let somma_orev (h1,m1)(h2,m2) = 
	if(verifica_ore_min (h1,m1) && verifica_ore_min (h2,m2)) then
	let mt = (m1+m2) mod 60 in
	let hm = (m1+m2)/60
	in let ht = h1+h2+hm
	in (ht mod 23,mt)
	else failwith "Formato scorretto";;

somma_orev (3,45)(4,20);;

(*soluzione prof*)
let somma_minuti m1 m2 =
  let m = m1+m2               (* sia m=m1+m2 *)
  in (m/60, m mod 60);;         (* nell'espressione (m/60, m mod 60) *)

let verifica (h,m) =
	h>=0 && h<=23 && m>=0 && m<=59;;

let somma_ore (h1,m1) (h2,m2) =
	if verifica (h1,m1) && verifica (h2,m2) then
	let (h3,m3) = (somma_minuti m1 m2)
	in ((h1+h2+h3) mod 24, m3)
	else failwith "Formato scorretto";;
  
(*esercizio 2*)
(*Definire funzioni che risolvano i problemi seguenti (le funzioni avranno il
nome e il tipo indicato all'inizio):
(a) (read_max: unit -> int) Leggere da tastiera una sequenza di nu-
meri interi (anche negativi), separati da Enter e terminata dalla strin-
ga vuota (o da una qualsiasi stringa che non rappresenti un intero),
e calcolarne il massimo. Se non viene immesso alcun numero, la
funzione sollever� un'eccezione.
(Tenere presente la funzione predefinita max: 'a -> 'a -> 'a).
(b) (read_max_min: unit -> int * int) Leggere da tastiera una se-
quenza di numeri interi (anche negativi), separati da Enter e termi-
nata dalla stringa vuota (o da una qualsiasi stringa che non rappre-
senti un intero), e calcolarne il massimo e il minimo. Se non viene
immesso alcun numero, la funzione sollever� un'eccezione.
(Tenere presente le funzione predefinite max: 'a -> 'a -> 'a
e min: 'a -> 'a -> 'a).
(c) (tutti_minori: int -> bool) Dato un intero n, leggere da tastiera
una sequenza di interi (anche negativi), separati da Enter e terminata
dalla stringa vuota (o da una qualsiasi stringa che non rappresenti
un intero), e determinare (riportando true o false) se i numeri letti
sono tutti minori di n. La funzione non deve mai sollevare eccezioni
e la sua esecuzione non deve terminare finch� non viene immessa la
stringa vuota (o non numerica).
(d) (occorre: int -> bool) Dato un intero n, leggere da tastiera una
sequenza di interi, separati da Enter e terminata dalla stringa vuota
(o da una qualsiasi stringa non numerica), e determinare (riportando
true o false) se n occorre nella sequenza. La funzione non deve mai
sollevare eccezioni e la sua esecuzione non deve terminare finch� non
viene immessa la stringa vuota (o non numerica).
(e) (num_di_stringhe: unit -> int) Leggere da tastiera una sequen-
za di stringhe non vuote, separate da Enter e terminata dalla stringa
1
vuota, e riportare la lunghezza della sequenza (il numero di strin-
ghe immesse, esclusa la stringa vuota che termina la sequenza). La
funzione non deve mai sollevare eccezioni.
(f) (stringa_max: unit -> string) Leggere da tastiera una sequenza
di stringhe non vuote, separate da Enter e terminata dalla stringa
vuota, e riportare la stringa di lunghezza massima. Se non viene
immessa nessuna stringa non vuota, la funzione riporter� la stringa
vuota.
(Tenere presente la funzione predefinita String.length: string ->
int).*)

let read_max () = 
	try
		let k = read_int()
		(*let rec aux m =
			let n = read_int()
			in aux (max m n)*)
		in k=k+5
	with _ -> failwith "cazzo fai" ;;

let read_max () =
  let rec aux n =
    try (* se non si immette un numero, viene sollevata un'eccezione
	   e il ciclo termina, riportando n *)
      let k = read_int()
      in aux (max n k)
    with _ -> n
  in try aux (read_int())
     with _ -> failwith "Sequenza vuota" ;;


let rec stampa_int n m =
	if n>m then ()
	else (print_int n; print_char '\n'; stampa_int (n+1) m);;

stampa_int 0 5;;

let rec ciclo n m =
if n>m then ()
else (print_int n;
print_newline();
ciclo (n+1) m)
let stampa = ciclo 0

let rec sommaseq () =
	let n = read_line ()
	in if n ="." then 0
	else (int_of_string n)+sommaseq ();;

let rec mcd n d =				(* mcd restituisce il massimo comun divisore *)
	if d = 0 then (abs n) (* abs appartiene ad ocaml e trova il modulo di un numero*)
	else mcd d (n mod d);;(* mod restituisce il resto di un numero diviso per un altro numero*)

let frazione n d =
	let x = mcd n d in
	(n/x),(d/x);;

frazione 10 4;;

(* fact: int -> int
fact n solleva l'eccezione NegativeNumber se n
� negativo,
altrimenti riporta il fattoriale di n
*)

exception NegativeNumber
let rec fact n =
if n<0 then raise NegativeNumber
else if n=0 then 1
else n*fact (n-1);;

fact (3);;
fact (-1);;

let estrazioni = 
  [[1; 7; 3]; [5; 4; 8]; [8; 7; 1]; [6; 10; 3]; [4; 2; 3]; [1; 5; 6];
   [8; 3; 3]; [7; 7; 2]; [2; 10; 8]; [3; 5; 6]; [4; 9; 7]; [1; 6; 3];
   [8; 4; 6]; [6; 3; 3]; [5; 6; 8]; [6; 7; 1]; [9; 5; 8]; [8; 1; 2];
   [10; 7; 1]; [7; 4; 6]];;

let rec estr_to_list = function
	[] -> []
	|	x::xs -> x @ (estr_to_list xs);; 		(* prende una lista di liste e la converte in una lista *)

let flatten = estr_to_list estrazioni;;

let rec inverti = function
	| [] -> []
	| x::xs -> (inverti xs) @ [x];;				(* prende lista e la inverte *)

inverti flatten;;

let rec copia_lista = function
	| [] -> []
	| x::xs -> [x] @ (copia_lista xs);;		(* copia una lista dentro un'altra lista *)

copia_lista flatten;;

let rec conta_occorrenze n = function
	[] -> 0
	| x::xs -> 
		if x=n then	1 + (conta_occorrenze n xs) 
	else
		conta_occorrenze n xs;;

conta_occorrenze 1 flatten;;

let rec conta_occ_tot = function
	| [] -> 0
	| x::rest -> (x, conta_occorrenze x lista)::(conta_occ_tot rest lista);;

conta_occ_tot flatten;;

let rec upto n m =
  if n>m then []
  else n::upto (n+1) m;;

upto 2 10;;

(* CREA NUM RANDOM TRA MIN E MAX COMPRESI *)
let rec numrand min max = 
	let r = Random.int max+1 in
	if r>=min then r
	else numrand min max;;

numrand 2 5;;

(* CREA LISTA DI DIMENSIONE DIM CON NUMERI RANDOM COMPRESI TRA 1 E MAX *) 
let rec crea_lista n dim max =
	if n>=dim then []
	else (numrand 1 max)::(crea_lista (n+1) dim max);;

let lista = crea_lista 0 3 10;;

let rec crea_lista_di_liste n dim_lst dim_lst_lst max =
	if n>=dim_lst_lst then []
	else (crea_lista 0 dim_lst max)::(crea_lista_di_liste (n+1) dim_lst dim_lst_lst max);;

crea_lista_di_liste 0 3 20 10;;

(************* LISTE ASSOCIATIVE *************)
let lst_assoc = [("giovanni",58);("palma",62);("tina",84);("vincenzo",87);("alfredo",90)];;

(* RICERCA DI UN ELEMENTO IN UNA LISTA ASSOCIATIVA *)
exception NotFound
(* assoc: 'a -> ('a * 'b) list -> 'b *) 
let rec assoc k = function 
	| [] -> raise NotFound 
	| (k1,v)::rest -> if k = k1 then v 
	else assoc k rest;;

assoc "vincenzo" lst_assoc;;

(* CANCELLA ELEMENTO DA LISTA ASSOCIATIVA *)
let rec cancella k = function
	| [] -> []
	| (k1,v)::rest ->if k1 = k then cancella k rest
	else (k1,v)::(cancella k rest);;

cancella "vincenzo" lst_assoc;;

(* SOSTITUZIONE ELEMENTO DENTRO UNA LISTA ASSOCIATIVA *)
let rec trova_e_sost k n = function
	| [] -> []
	| (k1,v)::rest -> if k = k1 then (k,n)::(trova_e_sost k n rest)
	else (k1,v)::trova_e_sost k n rest;;

trova_e_sost "vincenzo" 90 lst_assoc;;

(************* FUNZIONI DI ORDINE SUPERIORE *************)
let cons x xs = x::xs;;
let rec inits = function
	| [] -> []
	| x::xs -> List.map (cons x) ([]::inits xs);;

inits lista;;

let rec powerset = function
	| [] -> [[]]
	| x::xs -> let powxs = powerset xs in
							powxs @ List.map (cons x) powxs;;

powerset lista;;

(************* NUOVI TIPI *************)

type direzioni = Su | Giu | Destra | Sinistra;;



(************* ALBERI BINARI *************)

type 'a tree = Empty
  | Tr of 'a * 'a tree * 'a tree;;

let albero = 
    Tr(1, Tr(2, Tr(4,Empty,Empty), Empty), 
	  Tr(3, Tr(5, Tr(6,Empty,Empty), Tr(7,Empty,Empty)),
		Empty));;

let rec size = function
	| Empty -> 0
	| Tr(_,t1,t2) -> 1 + size t1 + size t2;;

size albero;;

type 'a ntree = Tr of 'a * 'a ntree list;;

let leaf a = Tr(a,[]);;

let albero =
	Tr(10,[Tr(8,[leaf 20;Tr(10,[leaf 5;leaf 4]);leaf 20]);Tr(5,[Tr(2,[leaf 1;leaf 8]);Tr(8,[leaf 3;leaf 10])])]);;

(* restituisce una lista senza doppioni da una lista *)
let rec mkset = function
    [] -> []
  | x::rest -> if List.mem x rest then mkset rest
      else x::mkset rest;;

(* restituisce una lista di nodi da un albero *)
let rec nodi (Tr(a,tlist)) =
	mkset(a::(List.flatten(List.map nodi tlist)));;

nodi albero;;

let rec visita (Tr(x,tlist)) = 
	 x::List.map visita tlist;;

visita albero;;
let rec figli n (Tr(a,tlist)) = 
	if n = a then 
		else figli n 
