print_string "ciao arianna\n";;
let numero = 6 * 7;;
let tot = succ (pred (pred numero));;
print_int tot;;
print_string "\n";;
let parola = if numero > 0 then "gino" else "pino";;
print_string parola;;
(*let double(x) = 2*x;;
let double = function x-> 2*x;;
(*let triple = function x-> 3*x;;*)
comp(double,function x -> 3*x);;*)

let rec gcd(m,n) = if m=0 then n
										else gcd(n mod m,m);;

let rec gcd =
	function (0,n) -> n
				 | (m,n) -> gcd(n mod m,m);;


(*somma*)
let somma a b = a+b;;
let s = somma 3 2;;
print_string "\nsomma = ";;
print_int s;;

(*-------------GRUPPO 1--------------*)

(*esercizio 1*)
let pi = 3.14159;;
let area x = pi *. x;;
let pi = 0.0;;
let x = "pippo";;

let a = area 3.0;;
print_char '\n';;
print_float a;;

let a b=3+b;;
a 5;;
a 7;;
let c = a 8;;
print_char '\n';;
print_int c;;

(*esercizio 2*)
let y = 100;;
let x = 5;;
let h x = y+x;;
let y = 0;;

let f = h 7;;
print_char '\n';;
print_int f;;

(*esercizio 3*)
let punct x = x = '.' || x =',' || x = ';';;

(*esercizio 4*)
let pi1 (x,_,_,_) = x;;
let pi2 (_,x,_,_) = x;;
let pi3 (_,_,x,_) = x;;
let pi4 (_,_,_,x) = x;;
let quadrupla = (5,('c',"antonio",(),if 3>4 then 0 else 1),"pippo",true);;
pi2 quadrupla;;
pi3 (pi2 quadrupla);;
pi4 (pi2 quadrupla);;
let quintupla = (3,'a',false,"ciao");;  
pi2 quintupla;; (*non funziona con 5 elementi perch� le funzioni pi1,pi2,pi3,pi4 accettano solo 4 elementi*)

(*esercizio 5*)
(*
E;;
not E;;
E && F;;
(not E) || F;;
E||F;;

*)
(*-------------GRUPPO 2--------------*)

(*----Esercizi 2-----*)
(*esercizio 1*)
let ultime_cifre x =
	let y=abs x in
	(((y/10) mod 10),
	y mod 10);;

ultime_cifre (5);;

(*esercizio 2*)
(* Una cifra � bella se � 0, 3, 7; un numero � bello se la sua ultima cifra � bella
e la penultima (se esiste) non lo �. Quindi in particolare le cifre belle sono
numeri belli. Definire un predicato bello: int -> bool che determini
se un numero � bello. La funzione non deve mai sollevare eccezioni, ma
riportare sempre un bool.*)
let cifra_bella x= x=0 || x=3 || x=7;;
let numero_bello n = 
	let y = n/10 mod 10 in
	(y=0 || (not(cifra_bella ((y))))) && cifra_bella (n mod 10);;

numero_bello 70;;

(*esercizio 3*)
let data (d,m) =
	d>0 &&
	match m with "novembre" | "aprile" | "giugno" | "settembre" -> d<=30
	| "febbraio" -> d<=28
	| "gennaio" | "marzo" | "maggio" | "luglio" | "agosto" | "ottobre" | "dicembre" -> d<=31
	| _-> false;;

data (29,"febbraio");;

(*-------------GRUPPO 3--------------*)
(*esercizio 1*)
(* Rappresentiamo le ore della giornata mediante coppie (h,m): int * int,
dove h � compreso tra 0 e 23, inclusi (le ore) e m � compreso tra 0 e 59,
inclusi (i minuti). Scrivere un programma con una funzione
somma_ore: (int * int) -> (int * int) -> int * int,
che calcoli la somma di due ore cos� rappresentate.
Ad esempio:
somma_ore (3,15) (4,20) = (7,35)
somma_ore (3,45) (4,20) = (8,5)
somma_ore (23,45)(0,20) = (0,5)
Se uno dei due argomenti non � la rappresentazione corretta di un'ora, la
funzione sollever� un'eccezione.
Ricordarsi, anche in questo esercizio, le funzioni predefinite di OCaml per
calcolare il modulo e la divisione intera.
*)
let verifica_ore_min (h,m) =
	h>=0 && h<24 && m>=0 && m<60;;

let somma_orev (h1,m1)(h2,m2) = 
	if(verifica_ore_min (h1,m1) && verifica_ore_min (h2,m2)) then
	let mt = (m1+m2) mod 60 in
	let hm = (m1+m2)/60
	in let ht = h1+h2+hm
	in (ht mod 23,mt)
	else failwith "Formato scorretto";;

somma_orev (3,45)(4,20);;

(*soluzione prof*)
let somma_minuti m1 m2 =
  let m = m1+m2               (* sia m=m1+m2 *)
  in (m/60, m mod 60);;         (* nell'espressione (m/60, m mod 60) *)

let verifica (h,m) =
	h>=0 && h<=23 && m>=0 && m<=59;;

let somma_ore (h1,m1) (h2,m2) =
	if verifica (h1,m1) && verifica (h2,m2) then
	let (h3,m3) = (somma_minuti m1 m2)
	in ((h1+h2+h3) mod 24, m3)
	else failwith "Formato scorretto";;
  
(*esercizio 2*)
(*Definire funzioni che risolvano i problemi seguenti (le funzioni avranno il
nome e il tipo indicato all'inizio):
(a) (read_max: unit -> int) Leggere da tastiera una sequenza di nu-
meri interi (anche negativi), separati da Enter e terminata dalla strin-
ga vuota (o da una qualsiasi stringa che non rappresenti un intero),
e calcolarne il massimo. Se non viene immesso alcun numero, la
funzione sollever� un'eccezione.
(Tenere presente la funzione predefinita max: 'a -> 'a -> 'a).
(b) (read_max_min: unit -> int * int) Leggere da tastiera una se-
quenza di numeri interi (anche negativi), separati da Enter e termi-
nata dalla stringa vuota (o da una qualsiasi stringa che non rappre-
senti un intero), e calcolarne il massimo e il minimo. Se non viene
immesso alcun numero, la funzione sollever� un'eccezione.
(Tenere presente le funzione predefinite max: 'a -> 'a -> 'a
e min: 'a -> 'a -> 'a).
(c) (tutti_minori: int -> bool) Dato un intero n, leggere da tastiera
una sequenza di interi (anche negativi), separati da Enter e terminata
dalla stringa vuota (o da una qualsiasi stringa che non rappresenti
un intero), e determinare (riportando true o false) se i numeri letti
sono tutti minori di n. La funzione non deve mai sollevare eccezioni
e la sua esecuzione non deve terminare finch� non viene immessa la
stringa vuota (o non numerica).
(d) (occorre: int -> bool) Dato un intero n, leggere da tastiera una
sequenza di interi, separati da Enter e terminata dalla stringa vuota
(o da una qualsiasi stringa non numerica), e determinare (riportando
true o false) se n occorre nella sequenza. La funzione non deve mai
sollevare eccezioni e la sua esecuzione non deve terminare finch� non
viene immessa la stringa vuota (o non numerica).
(e) (num_di_stringhe: unit -> int) Leggere da tastiera una sequen-
za di stringhe non vuote, separate da Enter e terminata dalla stringa
1
vuota, e riportare la lunghezza della sequenza (il numero di strin-
ghe immesse, esclusa la stringa vuota che termina la sequenza). La
funzione non deve mai sollevare eccezioni.
(f) (stringa_max: unit -> string) Leggere da tastiera una sequenza
di stringhe non vuote, separate da Enter e terminata dalla stringa
vuota, e riportare la stringa di lunghezza massima. Se non viene
immessa nessuna stringa non vuota, la funzione riporter� la stringa
vuota.
(Tenere presente la funzione predefinita String.length: string ->
int).*)

let read_max () = 
	try
		let k = read_int()
		(*let rec aux m =
			let n = read_int()
			in aux (max m n)*)
		in k=k+5
	with _ -> failwith "cazzo fai" ;;











let read_max () =
  let rec aux n =
    try (* se non si immette un numero, viene sollevata un'eccezione
	   e il ciclo termina, riportando n *)
      let k = read_int()
      in aux (max n k)
    with _ -> n
  in try aux (read_int())
     with _ -> failwith "Sequenza vuota" ;;
