let read_max () =
  let rec aux n =
    try (* se non si immette un numero, viene sollevata un'eccezione
	   e il ciclo termina, riportando n *)
      let k = read_int()
      in aux (max n k)
    with _ -> n
  in try aux (read_int())
  with _ -> failwith "Sequenza vuota";;
