(* fattoriale di n = n * (n-1) * .... * 1 
                   = n * (fattoriale di n-1) *)
(* fact: int -> int *)
let rec fact n =
  if n=0 then 1
  else n * fact (n-1)

(* ========================== *)
(* problema "evaluate" *)

(* numeric : char -> bool *)
let numeric c =
  c >= '0' && c <= '9'

(* loop: string -> int -> int *)
let rec loop s i =
  if not (numeric s.[i]) then i
  else loop s (i+1)

(* primo_non_numerico: string -> int *)
let primo_non_numerico s =
  loop s 0

(* dichiarazione locale di loop *)
let primo_non_numerico s =
  let rec loop s i =
    if not (numeric s.[i]) then i
    else loop s (i+1)
  in loop s 0

(* il parametro s di loop e' inutile *)
let primo_non_numerico s =
  (* loop: int -> int *)
  let rec loop i =
    if not (numeric s.[i]) then i
    else loop (i+1)
  in loop 0

(* sottoproblema substring *)
(* substring : string -> int -> int -> string *)
(* substring s j k = sottostringa di s che va dalla posizione j 
                           alla posizione k *)
let substring s j k =
  String.sub s j ((k-j)+1)

(* sottoproblema split_string *)
(* split_string : string -> int * char * int
   split_string s = (n,op,m) dove: n e' il primo operando, op l'operatore
                                   e m il secondo operando *)
let split_string s =
  let i = primo_non_numerico s
  in (int_of_string (substring s 0 (i-1)),
      s.[i],
      int_of_string (substring  s (i+1) 
		       ((String.length s)-1)))

(* problema principale *)
(* eccezione da sollevare nel caso in cui il carattere che rappresenta
   l'operatore non e' uno di quelli ammessi *)
exception BadOperation

(* evaluate: string -> int *)
let evaluate s =
  let (n,op,m) = split_string s
  in if op='+' then n+m
     else if op='-' then n-m
          else if op='*' then n*m
               else if op='/' then n/m
                    else raise BadOperation


(* ======================== *)
(* Dichiarazioni locali per evitare di calcolare
   piu' volte il valore di una stessa espressione *)

(* esempio: int * int * int -> int * int *)
let esempio (n,m,k) = ((n+m)/k, (n+m) mod k)

let esempio2(n,m,k) = 
   let somma = n+m 
   in (somma/k, somma mod k)

let esempio3(n,m,k) =
  (function somma -> (somma/k, somma mod k)) (n+m)

let esempio4(n,m,k) =
  let aux somma = (somma/k, somma mod k)
  in aux (n+m)

(* esempio: ridurre ai minimi termini una frazione rappresentata
   mediante una coppia di interi *)
(* gcd : int -> int -> int *)
(* gcd a b = massimo comun divisore di a e b *)
(*           assumendo che almeno uno tra a e b sia diverso da 0 *)
let rec gcd a b =
  if b=0 then (abs a)
  else gcd b (a mod b)

(* fraction : int * int -> int * int *)
let fraction (n,d) =
    let com = gcd n d
    in  (n/com, d/com)

(* ==== funzioni in forma currificata ==== *)
(* gcd : int -> int -> int *)
let rec gcd a b =
  if b=0 then (abs a)
  else gcd b (a mod b)

(* e' la forma currificata di *)
(* gcd2 : int * int -> int *)
let rec gcd2 (a,b) =
  if b=0 then (abs a)
  else gcd2 (b,a mod b)

(* altri esempi *)
(* mult: int * int -> int *)
let mult (m,n) = m * n
(* times:  int -> int -> int *)
let  times m n = m * n

(*  pair : 'a -> 'b -> 'a * 'b *)
let pair x y = (x,y)   

(* lessthan : 'a -> 'a -> bool *)
let lessthan x y = y < x

(* greaterthan : 'a -> 'a -> bool *)    
let greaterthan x y = y > x
    
(* equal : 'a -> 'a -> bool *)
let equal x y = x=y

    (* === definizione di operatori infissi *)
(* @@ : ('a -> 'b) -> ('c -> 'a) -> 'c -> 'b  *)
let (@@) f g x = f(g x)

    (* f: int -> int *)
let f = (times 2) @@ ((+) 100)


(* ======= Eccezioni ============= *)
(* fact: int -> int
   fact n solleva l'eccezione NegativeNumber se n 
          e' negativo,
   altrimenti riporta il fattoriale di n *)
exception NegativeNumber
    
let rec fact n =
  if n < 0 then raise NegativeNumber
                 (* viene "sollevata" l'eccezione *)
  else if n=0 then 1
  else n * fact (n-1)
      
(* primo_non_numerico: string -> int *)
let primo_non_numerico str =
  (* aux: int -> int *)
  let rec aux i =
      if not (numeric str.[i]) then i
      else aux (i+1)
  in 
  try aux 0
  with _ -> raise BadOperation

exception BadInt

(* split_string : string -> int * char * int *)
let split_string s =
  let i = primo_non_numerico s
  in 
  try (int_of_string (substring s 0 (i-1)),
       s.[i],
       int_of_string (substring  s (i+1) ((String.length s)-1)))
  with _ -> raise BadInt 

(* uso "sporco" delle eccezioni *)
(* conta_digits: string -> int *)
(* conta_digits s = numero di caratteri numerici in s *)
let conta_digits s =
  let max_index = (String.length s) - 1 in
  (* loop: int -> int *)
  (* loop i = numero di caratteri numerici in s a partire dalla
     posizione i *)
  let rec loop i =
    if i > max_index then 0 
    else if numeric s.[i] then 1 + loop (i+1)
         else loop (i+1) 
   in loop 0

let conta_digits2 s =
  let rec loop i =
    try if numeric s.[i] then 1 + loop (i+1)
        else loop (i+1)
    with Invalid_argument "index out of bounds" -> 0
  in loop 0

(* variabile muta *)
(* xor: bool * bool -> bool *)
let xor = function
   (true,false) | (false,true) -> true
  | _ -> false

(* ==== espressioni match ==== *)
(* uso di match *)
(* evaluate: string -> int *)
let evaluate s =
  let (n,op,m) = split_string s
  in match op with
    '+' -> n+m
  | '-' -> n-m
  | '*' -> n*m
  | '/' -> n/m
  | _ -> raise BadOperation
